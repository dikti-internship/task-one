package user

import (
	"context"
	"database/sql"
	"errors"
	"task-one/helpers"
	"task-one/user/model"
)

type UserRepository interface {
	Save(ctx context.Context, tx *sql.Tx, user model.User) model.User
	FindById(ctx context.Context, tx *sql.Tx, userId int) (model.User, error)
	FindByEmail(ctx context.Context, tx *sql.Tx, userEmail string) (model.User, error)
}

type UserRepositoryImpl struct {
}

func NewUserRepository() UserRepository {
	return &UserRepositoryImpl{}
}

func (repository *UserRepositoryImpl) Save(ctx context.Context, tx *sql.Tx, user model.User) model.User {
	query := "INSERT INTO customer(name,hash,email) values ($1,$2,$3) RETURNING id,name,email"
	row := tx.QueryRowContext(ctx, query, user.Name, user.Hash, user.Email)
	err := row.Scan(&user.Id, &user.Name, &user.Email)
	helpers.PanicIfError(err)

	return user
}

func (repository *UserRepositoryImpl) FindById(ctx context.Context, tx *sql.Tx, userId int) (model.User, error) {
	SQL := "SELECT id, name, email FROM customer WHERE id = $1"
	rows, err := tx.QueryContext(ctx, SQL, userId)
	helpers.PanicIfError(err)

	defer rows.Close()

	user := model.User{}
	if rows.Next() {
		err := rows.Scan(&user.Id, &user.Name, &user.Email)
		helpers.PanicIfError(err)
		return user, nil
	} else {
		return user, errors.New("user Not Found")
	}
}

func (repository *UserRepositoryImpl) FindByEmail(ctx context.Context, tx *sql.Tx, userEmail string) (model.User, error) {
	SQL := "SELECT id,name,email,hash FROM customer WHERE email = $1"
	rows, err := tx.QueryContext(ctx, SQL, userEmail)
	helpers.PanicIfError(err)

	defer rows.Close()

	user := model.User{}
	if rows.Next() {
		err := rows.Scan(&user.Id, &user.Name, &user.Email, &user.Hash)
		helpers.PanicIfError(err)
		return user, nil
	} else {
		return user, errors.New("user Not Found")
	}
}
