package user

import (
	"database/sql"
	"github.com/julienschmidt/httprouter"
	"task-one/helpers"
)

func RegisterRoute(router *httprouter.Router, db *sql.DB) {
	bcrypt := &helpers.BcryptImpl{}

	repository := NewUserRepository()
	service := NewUserService(repository, db, bcrypt)
	controller := NewUserController(service)

	router.GET("/users/:id", controller.FindById)
	router.POST("/users", controller.Create)
}
