package response

import "task-one/user/model"

type UserResponse struct {
	Id    int    `json:"id"`
	Email string `json:"email"`
	Name  string `json:"name"`
}

func ToUserResponse(user model.User) UserResponse {
	return UserResponse{
		Id:    user.Id,
		Email: user.Email,
		Name:  user.Name,
	}
}
