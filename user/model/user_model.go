package model

type User struct {
	Id    int
	Email string
	Name  string
	Hash  string
}
