package user

import (
	"context"
	"database/sql"
	"task-one/exception"
	"task-one/helpers"
	"task-one/user/dto"
	"task-one/user/model"
	"task-one/user/response"
)

type UserService interface {
	Create(ctx context.Context, request *dto.UserCreateDto) response.UserResponse
	FindById(ctx context.Context, userId int) response.UserResponse
}

type UserServiceImpl struct {
	Repository UserRepository
	DB         *sql.DB
	Bcrypt     *helpers.BcryptImpl
}

func NewUserService(repository UserRepository, db *sql.DB, bcrypt *helpers.BcryptImpl) UserService {
	return &UserServiceImpl{
		Repository: repository,
		DB:         db,
		Bcrypt:     bcrypt,
	}
}

func (service *UserServiceImpl) Create(ctx context.Context, request *dto.UserCreateDto) response.UserResponse {
	tx, err := service.DB.Begin()
	helpers.PanicIfError(err)

	defer helpers.CommitOrRollback(tx)

	hashChan := make(chan string)
	defer close(hashChan)

	go func() {
		hash, err := service.Bcrypt.HashPassword(request.Hash)
		helpers.PanicIfError(err)
		hashChan <- hash
	}()

	user := model.User{
		Name:  request.Name,
		Hash:  <-hashChan,
		Email: request.Email,
	}

	user = service.Repository.Save(ctx, tx, user)
	return response.ToUserResponse(user)

}

func (service *UserServiceImpl) FindById(ctx context.Context, userId int) response.UserResponse {
	tx, err := service.DB.Begin()
	helpers.PanicIfError(err)
	defer helpers.CommitOrRollback(tx)

	user, err := service.Repository.FindById(ctx, tx, userId)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	return response.ToUserResponse(user)
}
