package dto

type UserCreateDto struct {
	Name  string
	Email string
	Hash  string
}
