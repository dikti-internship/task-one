package user

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
	"task-one/helpers"
	"task-one/user/dto"
)

type UserController interface {
	Create(writer http.ResponseWriter, request *http.Request, params httprouter.Params)
	FindById(writer http.ResponseWriter, request *http.Request, params httprouter.Params)
}

type UserControllerImpl struct {
	Service UserService
}

func NewUserController(service UserService) UserController {
	return &UserControllerImpl{Service: service}
}

func (controller *UserControllerImpl) Create(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	userRequest := &dto.UserCreateDto{}
	helpers.ReadFromRequestBody(request, userRequest)

	data := controller.Service.Create(request.Context(), userRequest)
	result := helpers.ApiResponse{
		StatusCode: 201,
		Data:       data,
	}

	helpers.WriteToResponse(writer, result, 201)
}

func (controller *UserControllerImpl) FindById(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	userId := params.ByName("id")
	res, err := strconv.Atoi(userId)
	helpers.PanicIfError(err)

	data := controller.Service.FindById(request.Context(), res)
	result := helpers.ApiResponse{
		StatusCode: 200,
		Data:       data,
	}
	helpers.WriteToResponse(writer, result, 200)
}
