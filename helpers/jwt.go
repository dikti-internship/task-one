package helpers

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type Jwt interface {
	GenerateJwt(issuer string) (string, error)
	Parsejwt(cookie string) (string, error)
}

type JwtImpl struct {
}

func (j *JwtImpl) GenerateJwt(issuer string) (string, error) {
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    issuer,
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
	})
	env := GetConfig()
	return claims.SignedString([]byte(env.Jwt.SecretKey))
}

func (j *JwtImpl) Parsejwt(cookie string) (string, error) {
	env := GetConfig()
	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(env.Jwt.SecretKey), nil
	})
	if err != nil || !token.Valid {
		return "", err
	}
	claims := token.Claims.(*jwt.StandardClaims)
	return claims.Issuer, nil
}
