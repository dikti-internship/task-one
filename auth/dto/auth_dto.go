package dto

type AuthLoginDto struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Hash  string `json:"hash"`
	Id    int
}
