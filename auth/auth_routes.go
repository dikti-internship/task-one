package auth

import (
	"database/sql"
	"github.com/julienschmidt/httprouter"
	"task-one/helpers"
	"task-one/user"
)

func RegisterRoute(router *httprouter.Router, db *sql.DB) {
	bcrypt := &helpers.BcryptImpl{}
	jwt := &helpers.JwtImpl{}

	userService := user.NewUserRepository()
	authService := NewAuthService(userService, bcrypt, db, jwt)
	authController := NewAuthController(authService)

	router.POST("/login", authController.Login)

}
