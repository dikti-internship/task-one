package auth

import (
	"context"
	"database/sql"
	"fmt"
	"task-one/auth/dto"
	"task-one/auth/response"
	"task-one/exception"
	"task-one/helpers"
	"task-one/user"
)

type AuthService interface {
	Login(ctx context.Context, request *dto.AuthLoginDto) response.AuthResponse
}

type AuthServiceImpl struct {
	UserRepository user.UserRepository
	Bcrypt         *helpers.BcryptImpl
	DB             *sql.DB
	Jwt            *helpers.JwtImpl
}

func NewAuthService(userRepository user.UserRepository, bcrypt *helpers.BcryptImpl, DB *sql.DB, jwt *helpers.JwtImpl) AuthService {
	return &AuthServiceImpl{UserRepository: userRepository, Bcrypt: bcrypt, DB: DB, Jwt: jwt}
}

func (service *AuthServiceImpl) Login(ctx context.Context, request *dto.AuthLoginDto) response.AuthResponse {
	tx, err := service.DB.Begin()
	helpers.PanicIfError(err)
	defer helpers.CommitOrRollback(tx)

	user, err := service.UserRepository.FindByEmail(ctx, tx, request.Email)
	if err != nil {
		panic(exception.NewNotFoundError("account not found!"))
	}

	matchPassChan := make(chan bool)
	go func() {
		matchPassword := service.Bcrypt.CheckPasswordHash(request.Hash, user.Hash)
		matchPassChan <- matchPassword
	}()

	if matchPassword := <-matchPassChan; !matchPassword {
		panic(exception.NewNotFoundError("account not found!"))
	}

	userString := fmt.Sprintf("Id: %d, Name: %s, Hash: %s", user.Id, user.Name, user.Hash)
	token, err := service.Jwt.GenerateJwt(userString)
	helpers.PanicIfError(err)

	return response.AuthResponse{
		Id:    user.Id,
		Name:  user.Name,
		Email: user.Email,
		Token: token,
	}
}
