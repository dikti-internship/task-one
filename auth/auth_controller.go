package auth

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"task-one/auth/dto"
	"task-one/helpers"
)

type AuthController interface {
	Login(writer http.ResponseWriter, request *http.Request, params httprouter.Params)
}

type AuthControllerImpl struct {
	Service AuthService
}

func NewAuthController(service AuthService) AuthController {
	return &AuthControllerImpl{Service: service}
}

func (controller *AuthControllerImpl) Login(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	authRequest := &dto.AuthLoginDto{}
	helpers.ReadFromRequestBody(request, authRequest)

	data := controller.Service.Login(request.Context(), authRequest)
	result := helpers.ApiResponse{
		StatusCode: 200,
		Data:       data,
	}

	helpers.WriteToResponse(writer, result, 200)
}
