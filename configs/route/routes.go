package route

import (
	"github.com/julienschmidt/httprouter"
	"task-one/auth"
	"task-one/category"
	"task-one/configs/database"
	"task-one/exception"
	"task-one/product"
	"task-one/user"
)

func NewRouter() *httprouter.Router {
	var Router *httprouter.Router = httprouter.New()

	db := database.ConnectToDb()
	category.RegisterRoute(Router, db)
	product.RegisterRoute(Router, db)
	user.RegisterRoute(Router, db)
	auth.RegisterRoute(Router, db)

	Router.PanicHandler = exception.ErrorHandler
	return Router
}
